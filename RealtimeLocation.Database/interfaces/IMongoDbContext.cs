﻿using MongoDB.Driver;

namespace RealtimeLocation.Database.interfaces
{
    public interface IMongoDbContext
    {
        string ConnectionString { get; }
        string DatabaseName { get; }
        bool PluralizeDocumentName { get; set; }
        IMongoCollection<T> Collection<T>(string tableName = null);
    }
}

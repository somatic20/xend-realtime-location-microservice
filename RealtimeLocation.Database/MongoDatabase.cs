﻿using MongoDB.Driver;
using MongoDB.Driver.Linq;
using RealtimeLocation.Database.interfaces;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace RealtimeLocation.Database
{
    public class MongoDatabase<T>:IRepository<T> where T: class, new()
    {
        private static string connectionString;

        private static IMongoClient server = new MongoClient(connectionString);

        private string collectionName;

        private IMongoDatabase db;

        public MongoDatabase(string collection)
        {
            collectionName = collection;

            db = server.GetDatabase(MongoUrl.Create(connectionString).DatabaseName);
        }

        protected IMongoCollection<T> Collection
        {
            get
            {
                return db.GetCollection<T>(collectionName);
            }
            set
            {
                Collection = value;
            }
        }

        public IMongoQueryable<T> Query
        {
            get
            {
                return Collection.AsQueryable<T>();
            }
            set
            {
                Query = value;
            }
        }

        public T GetOne(Expression<Func<T, bool>> expression)
        {
            return Collection.Find(expression).SingleOrDefault();
        }

        public T FindOneAndUpdate(Expression<Func<T, bool>> expression, UpdateDefinition<T> update, FindOneAndUpdateOptions<T> option)
        {
            return Collection.FindOneAndUpdate(expression, update, option);
        }

        public void UpdateOne(Expression<Func<T, bool>> expression, UpdateDefinition<T> update)
        {
            Collection.UpdateOne(expression, update);
        }

        public void DeleteOne(Expression<Func<T, bool>> expression)
        {
            Collection.DeleteOne(expression);
        }

        public void SaveMany(IEnumerable<T> items)
        {
            Collection.InsertMany(items);
        }

        public void Save(T item)
        {
            Collection.InsertOne(item);
        }
        

        //protected MongoDbContext _bbContext;
        //public Repository(MongoDbContext dbContext)
        //{
        //    _bbContext = dbContext;
        //}

        //public void Add<T>(T item) where T:class, new()
        //{
        //    _bbContext.Collection<T>().InsertOne(item);
        //}

        //public void Update<T>(object obj, Expression<Func<T, bool>> expression) where T : class
        //{
        //    _bbContext.Collection<T>(nameof(T)).UpdateOne(Builders<T>.Update));
        //}

        //public async Task<IEnumerable<T>> Find<T>(Expression<Func<T, bool>> expression) where T : class
        //{
        //    return (await _bbContext.Collection<T>(nameof(T))
        //        .FindAsync(Builders<T>.Filter.Where(expression))).ToEnumerable();
        //}



    }
}

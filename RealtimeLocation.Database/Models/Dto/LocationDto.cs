﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RealtimeLocation.Database.Models.Dto
{
    public class LocationDto
    {
        public int Id { get; set; }
        public string LocationName { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public int Count { get; set; }
    }
}

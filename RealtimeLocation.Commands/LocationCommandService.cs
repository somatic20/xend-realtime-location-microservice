﻿using MongoDB.Driver;
using RealtimeLocation.Commands.interfaces;
using RealtimeLocation.Database;
using RealtimeLocation.Database.interfaces;
using RealtimeLocation.Database.Models.Dto;
using RealtimeLocation.Database.Models.Entity;
using RealtimeLocationService.Common;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RealtimeLocation.Commands
{
    public class LocationCommandService : ILocationCommandService
    {
        private readonly IMongoCollection<Location> _collection;
        private readonly MongoDatabase<Location> database;
        public LocationCommandService(IMongoDbContext context)
        {
            _collection = context.Collection<Location>(Constants.SavedLocationCollectionName);
        }
        public Location AddLocation(LocationDto locationDto)
        {
            var singleLocation = _collection.Find(p => p.LocationName == locationDto.LocationName).SingleOrDefault();
            if (singleLocation == null)
            {
                Location location = new Location
                {
                    Latitude = locationDto.Latitude,
                    LocationName = locationDto.LocationName,
                    Longitude = locationDto.Longitude
                };
                database.Save(location);
                return location;
            }
            return null;

        }

        public Location UpdateLocation(LocationDto locationDto)
        {
            throw new NotImplementedException();
        }
    }
}

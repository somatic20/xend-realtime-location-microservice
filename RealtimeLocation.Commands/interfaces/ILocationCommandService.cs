﻿using RealtimeLocation.Database.Models.Dto;
using RealtimeLocation.Database.Models.Entity;
using System.Threading.Tasks;

namespace RealtimeLocation.Commands.interfaces
{
    interface ILocationCommandService
    {
        Location AddLocation(LocationDto locationDto);
        Location UpdateLocation(LocationDto locationDto);
    }
}

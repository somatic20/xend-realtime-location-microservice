﻿using RealtimeLocation.Database.Models.Entity;
using System.Collections.Generic;

namespace RealtimeLocation.Queries.interfaces
{
    public interface ILocationQueryService
    {
        IEnumerable<Location> GetLocations();
    }
}

﻿using MongoDB.Driver;
using RealtimeLocation.Database;
using RealtimeLocation.Database.interfaces;
using RealtimeLocation.Database.Models.Dto;
using RealtimeLocation.Database.Models.Entity;
using RealtimeLocation.Queries.interfaces;
using RealtimeLocationService.Common;
using System;
using System.Collections.Generic;

namespace RealtimeLocation.Queries
{
    public class LocationQueryService : ILocationQueryService
    {
        private readonly IMongoCollection<Location> _collection;
        private readonly MongoDatabase<Location> database;
        public LocationQueryService(IMongoDbContext dbContext)
        {
            _collection = dbContext.Collection<Location>(Constants.SavedLocationCollectionName);
        }

        public IEnumerable<Location> GetLocations()
        {
            throw new NotImplementedException();
        }
    }
}
